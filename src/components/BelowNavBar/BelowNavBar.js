import React from "react";
import { Switch, Route } from "react-router-dom";
import Main from "../../components/Main/Main.js";
import Car from "../../components/Car/Car.js";
import ModelSImage from "../../components/Car/models.jpg";
import Model3Image from "../../components/Car/model3.jpg";
import ModelXImage from "../../components/Car/modelx.jpg";
import ModelYImage from "../../components/Car/modely.jpg";

export default class BelowNavBar extends React.Component {
  render() {
    return (
      <div style={{ marginTop: this.props.navBarHeight }}>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route
            exact
            path="/models"
            render={(props) => <Car {...props} image={ModelSImage} />}
          />

          <Route
            exact
            path="/model3"
            render={(props) => <Car {...props} image={Model3Image} />}
          />
          <Route
            exact
            path="/modelx"
            render={(props) => <Car {...props} image={ModelXImage} />}
          />
          <Route
            exact
            path="/modely"
            render={(props) => <Car {...props} image={ModelYImage} />}
          />
        </Switch>
      </div>
    );
  }
}

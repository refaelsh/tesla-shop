import React from "react";
import "./car.css";

export default class Car extends React.Component {
  render() {
    return (
      <div
        className="car-div"
        style={{
          backgroundImage: `url(${this.props.image})`,
        }}
      ></div>
    );
  }
}

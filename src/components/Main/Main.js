import React from "react";
import main from "./main.jpg";
import "./main.css";

export default class Main extends React.Component {
  render() {
    return (
      <div
        className="background-div"
        style={{
          backgroundImage: `url(${main})`,
        }}
      ></div>
    );
  }
}

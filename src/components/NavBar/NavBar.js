import React from "react";
import { Link } from "react-router-dom";
import "./navbar.css";

export default class NavBar extends React.Component {
  render() {
    return (
      <div id="navBarID" className="nav-bar-div">
        <Link className="nav-bar-item" to="/">
          Main
        </Link>
        <Link className="nav-bar-item" to="/models">
          Model S
        </Link>
        <Link className="nav-bar-item" to="/model3">
          Model 3
        </Link>
        <Link className="nav-bar-item" to="/modelx">
          Model X
        </Link>
        <Link className="nav-bar-item" to="/modely">
          Model Y
        </Link>
      </div>
    );
  }
}
